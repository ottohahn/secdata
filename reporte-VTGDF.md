# Timeline of reports of Vantage Drilling International (VTGDF)

Otto Hahn Herrera Ph. D.

## Table of contents

- Summary
- Introduction
- Analysis
- Results
- Conclusions

## Summary

The following sections detail the analysis realized on the SEC documentation
for Vantage Drilling International (ticker symbol: VTGDF). For the analysis 731
files were downloaded from sec.gov/edgar. From the analysis the following
results appear:

TBD

## Introduction

Yhis is the current state of Vantage drilling international.
In the analysis we will cover the period (2007-2018).

Vantage Drilling International provides offshore contract drilling services in Congo, Malaysia, Indonesia, and internationally. It also provides construction supervision services for drilling units owned by others. The company primarily serves oil and natural gas companies. As of March 28, 2018, it had a fleet of three ultra-deepwater drillships; and four premium jackup drilling rigs. The company was formerly known as Offshore Group Investment Limited and changed its name to Vantage Drilling International in February 2016. Vantage Drilling International was founded in 2007 and is based in Houston, Texas.

### People:

(Board of directors retrieved 2018)
Tom Bates
Tom Bates has 40 years of operational experience in the oil and gas industry, having held executive leadership positions at several major energy companies. He previously served as Group President at Baker Hughes, Chief Executive Officer at Weatherford-Enterra, and President of the Anadrill division of Schlumberger. A current director of TETRA Technologies, Mr. Bates also has served as Chairman of the Board of Hercules Offshore, Inc. in addition to serving on several other public company energy-related boards. He also was Managing Director and then Senior Advisor at Lime Rock Partners, an energy-focused private equity investment firm. Mr. Bates has a doctorate in mechanical engineering from the University of Michigan. He is currently an Adjunct Professor and co-chair of the advisory board for the Energy MBA Program at the Neeley School of Business at Texas Christian University in Fort Worth. Mr. Bates serves as Chairman of the Board.

Matthew Bonanno
Matthew Bonanno joined York Capital Management in July 2010 and is a Partner of the Firm. Mr. Bonanno joined York from the Blackstone Group where he worked as an Associate focusing on restructuring, recapitalization and reorganization transactions. Prior to joining the Blackstone Group, Mr. Bonanno worked on financing and strategic transactions at News Corporation and as an Investment Banker at JP Morgan and Goldman Sachs. Mr. Bonanno is currently a member of the Board, in his capacity as a York employee, of Rever Offshore AS and all entities incorporated pursuant to York’s partnership with Costamare Inc. and Augustea Bunge Maritime. Mr. Bonanno received a B.A. in History from Georgetown University and an M.B.A in Finance from The Wharton School of the University of Pennsylvania. Mr. Bonanno serves on the Board’s Compensation Committee.

Esa Ikaheimonen
Esa Ikaheimonen is the former Executive Vice President and Chief Financial Officer of Transocean Ltd. He previously served as Chief Executive Officer of Seadrill’s Asia Offshore Drilling subsidiary and Senior Vice President and Chief Financial Officer of Seadrill Ltd. Mr. Ikaheimonen previously served as Executive Vice President and Chief Financial Officer of Poyry PLC, a global consulting and engineering company. Prior to Poyry, he was employed by Royal Dutch Shell for almost 20 years where he held several senior positions including Regional Vice President Finance for Upstream in both Africa and the Middle-East, Finance and Commercial Director in Qatar, and Downstream Finance Director for Scandinavia. He has served on the board of directors of Transocean Partners, LLC, as Chairman of the Board, and as an independent director of Ahlstrom Plc where he served as Chairman of the Audit Committee. Mr. Ikaheimonen earned a Masters Degree in Law from the University of Turku in Finland, specializing in Tax Law and Tax Planning. Mr. Ikaheimonen serves as chairman of the Board’s Audit Committee.

Nils E. Larsen
Nils E. Larsen is a Senior Operating Adviser working with The Carlyle Group’s U.S. Equity Opportunities Fund. Prior to partnering with The Carlyle Group, Mr. Larsen served in a variety of senior executive positions with Tribune Company, including the President and Chief Executive Officer of Tribune Broadcasting and as Co-President of Tribune Company. Before joining Tribune Company, Mr. Larsen served as a Managing Director of Equity Group Investments, LLC focusing on investments in the media, transportation, energy, retail grocery and member loyalty and rewards sectors. Mr. Larsen started his career at CS First Boston where he focused on the capital requirements and derivative products needs of U.S. financial institutions and non-U.S. based entities. Mr. Larsen has significant governance experience in post-bankrupt entities. Mr. Larsen received his A.B. summa cum laude from Bowdoin College. Mr. Larsen serves on the Audit Committee of the Board.

Scott McCarty
Scott McCarty is a partner of Q Investments and has been with Q since 2002. Prior to his current position as manager of the venture capital, private equity, and distressed investment groups, he was a portfolio manager. Before joining Q Investments, Mr. McCarty was a captain in the United States Army and worked in the office of U.S. Senator Kay Bailey Hutchison. Mr. McCarty graduated with a BS from the United States Military Academy at West Point, where he was a Distinguished Cadet and recipient of the General Lee Donne Olvey Award, and earned an MBA from Harvard Business School. Mr. McCarty serves on the Board’s Compensation Committee.

L. Spencer Wells
L. Spencer Wells is a Founder and Partner of Drivetrain Advisors, a provider of fiduciary services to members of the alternative investment community, with a particular expertise in restructuring and turnarounds. Previously, he served as a senior advisor and partner with TPG Special Situations Partner where he helped manage a $2.5B portfolio of liquid and illiquid distressed credit investments employing a “distressed for control” investment strategy. Mr. Wells previously served as a Partner for Silverpoint Capital where he managed a $1.3B investment portfolio consisting primarily of stressed and distressed bank loans and bonds focusing on the Oil and Gas Exploration and Production, Oilfield Services, Power Generation, Financial Institutions and Chemicals industries, among others. He previously served as an Analyst on the Distressed Debt Desks at Union Bank of Switzerland, Deutsche Bank and Bankers Trust. Mr. Wells has served on numerous public and private company boards including Advanced Emissions Solutions, Inc., Syncora Guarantee, Ltd., and Affinion Group, Inc., among others. Mr. Wells received his Bachelor of Arts degree from Wesleyan University and his MBA from the Columbia Business School. Mr. Wells serves as chairman of the Board’s Compensation Committee.

------
Managment

Ihab Toma
Chief Executive Officer
Mr. Toma has served as our Chief Executive Officer since August 29, 2016.

Prior to joining Vantage Mr. Toma served in the following capacities:

Transocean
Executive Vice President – Chief of Staff (2012 - 2013)
Executive Vice President – Operations (2011 - 2012)
Executive Vice President – Global Business (2010 - 2011)
Senior Vice President – Marketing and Planning (2009 - 2010)

Schlumberger 
Various Roles (1986 - 2009)

----

Douglas Halkett
Chief Operating Officer
Mr. Halkett has served as our Chief Operating Officer since January 15, 2008.

Prior to joining Vantage Mr. Halkett served in the following capacities:

Transocean
Division Manager, Northern Europe
(2003 – 2007)

Operations Manager, Gulf of Mexico
(2001 – 2003) & in UK (1996 – 2001)

Forasol-Foramer
Various Operations & Business Roles
(1988 – 1996)

----

Tom Cimino
Chief Financial Officer
Mr. Cimino has served as our Chief Financial Officer since September 22, 2016.

Prior to joining Vantage Mr. Cimino served in the following capacities:

AEI Services LLC – an international energy infrastructure firm
Chief Financial Officer (2012-2016)

Vice President - Controller (2007-2012)

PricewaterhouseCoopers 
Director – Global Capital Markets Group (2003 -2007)

Mr. Cimino started his career in public accounting with KPMG and also worked with the U.S Securities and Exchange Commission, Division of Corporate Finance.

----

Bill Thomson
Vice President Marketing & Business Development
Mr. Thomson has served as our Vice President Marketing and Business Development since July 2016.

Prior to this he served as our Vice President of Technical services, Supply chain and Projects from March 8, 2008 until July 2016.

Prior to joining Vantage Mr. Thomson served in the following capacities:

Transocean
Asset Operations Manager UK, Europe and Africa Technical Support Manager, New Build Project Manager and other various roles in Engineering, Project Management & Technical Support.
(1993-2008)

----

Wayne Bauer
Director of QHSE
Mr. Bauer joined the Company in April 2014 as our Southeast Asian QHSE Manager. He has served as our Director of QHSE since 1 December 2016.

Prior to joining Vantage Mr. Bauer served in the following capacities:

RPS Energy
Principle Safety and Risk Consultant – Australasia (2011-2014)

COSL Drilling Pan Pacific
QHSE Manager (2009-2010)

WorleyParsons Services
Safety Consultancy Manager (2006-2009)

----

Derek Massie
Vice President of Human Resources
Mr. Massie joined the Company in 2017 and has served as our Vice President of Human Resources since 1 January 2018.

Prior to joining Vantage Mr. Massie served in the following capacities:

Dynamic People Strategies Ltd
Independent Consultant; Executive Coach and Interim Leader (2016-2017)

Maxwell Drummond International
Principal Consultant; Executive Search and Consultancy (2013-2016)

Seadrill Management AS.
Senior Vice President Human Resources and Internal Communications (2010-2013)

Acergy MS Ltd

HR Director Corporate and Offshore (2006-2010)

----

Douglas Stewart
Vice President, General Counsel & Corporate Secretary
Douglas Stewart has served as our Vice President, General Counsel and Corporate Secretary since June 2016 and our Chief Compliance Officer since December 12, 2016.

Prior to joining Vantage Mr. Stewart served in the following capacities:

Stallion Oilfield Holdings, Inc 
Executive Vice President, General Counsel and Secretary (2007-2016)

Occidental Petroleum Corporation
International business development group.

Vinson & Elkins LLP
Corporate finance and securities law.

----

Linda J. Ibrahim
Vice President of Tax & Governmental Compliance
Ms. Ibrahim joined the Company in 2010 and has served as our Vice President of Tax & Governmental Compliance since 1 January 2015.

Prior to joining Vantage Ms. Ibrahim served in the following capacities:

Pride International
Assistant Tax Director – Western Hemisphere (2006-2010)

Pricewaterhouse Coopers LLP
Tax Manager (1999-2006)

BDO
Tax Senior (1997-1999)

----

## Analysis

The analysis can be divided in two parts, news and SEC documentation

### News Analysis



### SEC documentation analysis



