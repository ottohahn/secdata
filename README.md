# secdata

Repository of scripts and databases for the analysis of SEC data for Vantage 
Drilling International VTGDF.

## Contents

The repository contains folders containing the SEC files and scripts to extract
and process information from them:

- Report: reporte-VTGDF.md
- SQLite DB:
- Script with regexes for getting names, dates, companies, amounts:
- Script for getting stock percentages:

## Methodology

The idea is to create a database to store the file names, the dates of
creation, the type of form submitted, and then extract names, companies, stock
amounts, money amounts, and ship names to create a summary timeline which will
be added to the report document.


